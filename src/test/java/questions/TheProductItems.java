package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.thucydides.core.annotations.Step;
import ui.SearchBox;

import java.util.List;

public class TheProductItems implements Question<List<String>> {
    @Override
    @Step
    public List<String> answeredBy(Actor actor) {
        return Text.of(SearchBox.ITEMS).viewedBy(actor).asList();
    }
    public static Question<List<String>> displayed(){
        return new TheProductItems();
    }
}
