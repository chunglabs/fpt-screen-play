package steps;


import Utils.DataShare;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.collection.IsArray;
import org.hamcrest.collection.IsArrayContainingInOrder;
import org.hamcrest.collection.IsArrayWithSize;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsCollectionContaining;
import org.openqa.selenium.WebDriver;
import questions.TheProductItems;
import tasks.OpenTheWeb;
import tasks.Search;


import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

public class SearchSteps {
    @Managed
    public WebDriver hisBrowser;

    @Steps
    OpenTheWeb openTheWeb;


    Actor chung;

    @Before
    public void set_the_stage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^\"([^\"]*)\" opened the Fpt Shop Homepage$")
    public void something_opened_the_fpt_shop_homepage(String actorName) throws Throwable {
        chung = Actor.named(actorName);
        chung.can(BrowseTheWeb.with(hisBrowser));
        chung.wasAbleTo(openTheWeb);
    }

    @When("^He search for \"([^\"]*)\" product$")
    public void he_search_for_something_product(String productName) throws Throwable {
        DataShare.PRODUCT = productName;
        chung.attemptsTo(Search.forTheProduct(productName));
        hisBrowser.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    @Then("^\"([^\"]*)\" results will be show$")
    public void something_results_will_be_show(String numOfResults) throws Throwable {
        chung.should(eventually(seeThat(TheProductItems.displayed(),IsCollectionWithSize.hasSize(Integer.parseInt(numOfResults)) )));
        chung.should(eventually(seeThat(TheProductItems.displayed(),hasItem(containsString(DataShare.PRODUCT)) )));
    }

}
