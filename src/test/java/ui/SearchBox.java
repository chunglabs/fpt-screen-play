package ui;

import net.serenitybdd.screenplay.targets.Target;

public class SearchBox {
    public static Target SEARCH_FIELD = Target.the("search field")
            .locatedBy(".fs-search input");
    public static Target ITEMS = Target.the("")
            .locatedBy(".fs-sresult li h3" );
}
