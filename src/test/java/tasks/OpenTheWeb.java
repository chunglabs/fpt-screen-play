package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import ui.FptShopHomePage;

public class OpenTheWeb implements Task {
    FptShopHomePage fptShopHomePage;

    @Override
    @Step
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(fptShopHomePage)
        );
    }
}
