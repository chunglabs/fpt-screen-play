package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import ui.SearchBox;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Search implements Task {
    private final String SearchProduct;

    public Search(String searchProduct) {
        this.SearchProduct = searchProduct;
    }

    @Override
    @Step
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(SearchProduct).into(SearchBox.SEARCH_FIELD)
        );
    }

    public static Search forTheProduct(String searchProduct){
        return instrumented(Search.class,searchProduct);
    }
}
